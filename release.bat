@echo on
set versionnumber=0.7b1
md release%versionnumber%
cd release%versionnumber%
md GameData
md GameData\KSRSS
md Extras
cd ..
xcopy /s KSRSS release%versionnumber%\GameData\KSRSS
xcopy /s Extras release%versionnumber%\Extras
ECHO F|xcopy GPL release%versionnumber%\GPL
ECHO F|xcopy README.md release%versionnumber%\README.md
SET ZIP="C:\Program Files\7-Zip\7z.exe"
%ZIP% a KSRSS-%versionnumber%.zip -tzip -r .\release%versionnumber%\GameData
%ZIP% a KSRSS-%versionnumber%.zip -tzip -r .\release%versionnumber%\Extras
%ZIP% a KSRSS-%versionnumber%.zip -tzip -r .\release%versionnumber%\GPL
%ZIP% a KSRSS-%versionnumber%.zip -tzip -r .\release%versionnumber%\README.md
rd /s /q release%versionnumber%