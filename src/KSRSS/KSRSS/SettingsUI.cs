﻿using System;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using UnityEngine;
using KSP.UI.Screens;
using KSP.Localization;

namespace KSRSS
{
    [KSPAddon(KSPAddon.Startup.MainMenu, false)]
    public class MainMenu : MonoBehaviour
    {
        public void Start()
        {
            SettingsUI ui = new SettingsUI();
            ui.Show();
        }
    }

    [KSPAddon(KSPAddon.Startup.MainMenu, true)]
    public class AppLauncherButton : MonoBehaviour
    {
        private Texture2D launcherButtonTexture;
        private Texture2D launcherButtonTexture2;
        private ApplicationLauncherButton launcherButton;
        private const ApplicationLauncher.AppScenes APP_SCENES =
            ApplicationLauncher.AppScenes.FLIGHT |
            ApplicationLauncher.AppScenes.SPH |
            ApplicationLauncher.AppScenes.VAB |
            ApplicationLauncher.AppScenes.MAPVIEW |
            ApplicationLauncher.AppScenes.SPACECENTER |
            ApplicationLauncher.AppScenes.TRACKSTATION;

        private SettingsUI ui = null;

        private bool firstTime = true;
        private bool isOpen;

        private void Setup()
        {
            launcherButtonTexture = LoadTextureDXT(KSPUtil.ApplicationRootPath + "GameData/KSRSS/Textures/button1.dds",
                TextureFormat.DXT5);
            launcherButtonTexture2 = LoadTextureDXT(KSPUtil.ApplicationRootPath + "GameData/KSRSS/Textures/button2.dds",
                TextureFormat.DXT5);
        }
        
        private void Awake()
        {
            GameEvents.onGUIApplicationLauncherReady.Add(AddButton);
            GameEvents.onGUIApplicationLauncherUnreadifying.Add(RemoveButton);
        }

        private void AddButton()
        {
            if (!ApplicationLauncher.Ready) return;
            if (firstTime)
            {
                Setup();
                firstTime = false;
            }

            if (launcherButton == null)
            {
                launcherButton = ApplicationLauncher.Instance.AddModApplication(Show, Hide, EmptyCallback,
                    EmptyCallback, EmptyCallback, EmptyCallback, APP_SCENES, launcherButtonTexture);
            }
        }

        private void EmptyCallback()
        {
            
        }

        private void RemoveButton(GameScenes scenes)
        {
            if (launcherButton != null)
            {
                GoAway();
            }
        }

        private void GoAway()
        {
            if (isOpen) Hide();

            try
            {
                if (launcherButton != null && ApplicationLauncher.Instance != null)
                {
                    ApplicationLauncher launcher = ApplicationLauncher.Instance;
                    launcher.RemoveModApplication(launcherButton);
                    launcherButton = null;
                }
            }
            catch (Exception e)
            {
                Utils.Log("Failed unregistering AppLauncher handlers," + e.Message);
            }
        }

        private void Show()
        {
            if (ui == null)
            {
                ui = new SettingsUI();
            }
            ui.Show();
            isOpen = true;
            launcherButton.SetTexture(launcherButtonTexture2);
        }

        private void Hide()
        {
            if (ui.popup == null)
                return;
            ui.CloseDialog();
            isOpen = false;
            launcherButton.SetTexture(launcherButtonTexture);
        }
        
        public Texture2D LoadTextureDXT(string ddsFilePath, TextureFormat textureFormat)
        {
            Texture2D texture = null;
            try
            {
                if(!File.Exists(ddsFilePath))
                    Utils.Log(string.Format(
                        "File does not exist: ", ddsFilePath));

                byte[] ddsBytes = File.ReadAllBytes(ddsFilePath);

                if (textureFormat != TextureFormat.DXT1 && textureFormat != TextureFormat.DXT5)
                    Utils.Log(string.Format(
                        "Invalid TextureFormat.Only DXT1 and DXT5 formats are supported by this method at path: ", ddsFilePath));

                byte ddsSizeCheck = ddsBytes[4];

                if (ddsSizeCheck != 124)
                    Utils.Log(string.Format("Invalid DDS DXTn texture. Unable to read: {0}", ddsFilePath));  //this header byte should be 124 for DDS image files

                int height = ddsBytes[13] * 256 + ddsBytes[12];
                int width = ddsBytes[17] * 256 + ddsBytes[16];

                int DDS_HEADER_SIZE = 128;
                byte[] dxtBytes = new byte[ddsBytes.Length - DDS_HEADER_SIZE];
                Buffer.BlockCopy(ddsBytes, DDS_HEADER_SIZE, dxtBytes, 0, ddsBytes.Length - DDS_HEADER_SIZE);

                texture = new Texture2D(width, height, textureFormat, false);
                texture.LoadRawTextureData(dxtBytes);
                texture.Apply();
            } 
            catch (Exception ex)
            {
                Utils.Log(string.Format("Exception in LoadTextureDXT: {0}", ex.ToString()));
            }

            return texture;
        }
        
    }
    public class SettingsUI
    {
        public PopupDialog popup;
        private DialogGUIToggleGroup KSRSSGroup;
        private DialogGUIToggleGroup VEGroup;
        private ConfigNode settingsNode;
        private ConfigNode fileNode;
        private static readonly string patch4k = KSPUtil.ApplicationRootPath + "GameData/KSRSS/Patches/4k.disabled";
        private static readonly string patch4kEnabled = patch4k.Replace(".disabled", ".cfg");
        private static readonly string patch16k = KSPUtil.ApplicationRootPath + "GameData/KSRSS/Patches/16k.disabled";
        private static readonly string patch16kEnabled = patch16k.Replace(".disabled", ".cfg");
        private static readonly string patch32kVE = KSPUtil.ApplicationRootPath + "GameData/KSRSS/Patches/32kVE.disabled";
        private static readonly string patch32kVEEnabled = patch32kVE.Replace(".disabled", ".cfg");
        //private string choice = "8k";
        private bool[] sets = new bool[5] {false, false, false, false, false};

        public Rect geometry = Rect.zero;
        
        // TODO: Localization
        public void Show()
        {
            if (geometry == Rect.zero)
                geometry = new Rect(0.25f, 0.75f, 300f, 100f);
            if (File.Exists(KSPUtil.ApplicationRootPath + "GameData/KSRSS/Patches/4k.cfg"))
            {
                //choice = "4k";
                sets[0] = true;
            } 
            else if (File.Exists(KSPUtil.ApplicationRootPath + "GameData/KSRSS/Patches/16k.cfg"))
            {
                sets[2] = true;
            }
            else
            {
                sets[1] = true;
            }

            if (File.Exists(KSPUtil.ApplicationRootPath + "GameData/KSRSS/Patches/32kVE.cfg"))
            {
                sets[4] = true;
            }
            else
            {
                sets[3] = true;
            }
            fileNode = ConfigNode.Load(KSPUtil.ApplicationRootPath + "GameData/KSRSS/resolution.cfg");
            settingsNode = fileNode.GetNode("KSRSSSETTINGS");
            //settingsNode.SetValue("textureRes", "ere");
            KSRSSGroup = new DialogGUIToggleGroup(
                new DialogGUIToggle(sets[0], "4k", Toggle4k),
                new DialogGUIToggle(sets[1], "8k", Toggle8k),
                new DialogGUIToggle(sets[2], "16k", Toggle16k));
            VEGroup = new DialogGUIToggleGroup(
                new DialogGUIToggle(sets[3], "8k", ToggleVE8k),
                new DialogGUIToggle(sets[4], "32k", ToggleVE32k));
            MultiOptionDialog mod;
            if (Directory.Exists(KSPUtil.ApplicationRootPath + "GameData/EnvironmentalVisualEnhancements"))
            {
                mod = new MultiOptionDialog("KSRSSSettings",
                Localizer.Format("#LOC_KSRSS_UI_Msg"),
                Localizer.Format("#LOC_KSRSS_UI_Label1"),
                HighLogic.UISkin, geometry,
                new DialogGUIFlexibleSpace(),
                new DialogGUIVerticalLayout(
                    new DialogGUIFlexibleSpace(),
                    new DialogGUILabel(Localizer.Format("#LOC_KSRSS_UI_Label1"), true),
                    new DialogGUIHorizontalLayout(KSRSSGroup),
                    new DialogGUIFlexibleSpace(),
                    new DialogGUILabel(Localizer.Format("#LOC_KSRSS_UI_Label2")),
                    new DialogGUIHorizontalLayout(VEGroup),
                    new DialogGUIFlexibleSpace(),
                    new DialogGUILabel(Localizer.Format("#LOC_KSRSS_UI_Label3")),
                    new DialogGUIHorizontalLayout(new DialogGUIButton(Localizer.Format("#LOC_KSRSS_UI_Button"), CloseDialog, false)),
                    new DialogGUIFlexibleSpace()
                ));
            }
            else
            {
                mod = new MultiOptionDialog("KSRSSSettings",
                    "Use this window to change your KSRSS textures resolution",
                    "KSRSS Settings",
                    HighLogic.UISkin, geometry,
                    new DialogGUIFlexibleSpace(),
                    new DialogGUIVerticalLayout(
                        new DialogGUIFlexibleSpace(),
                        new DialogGUILabel("KSRSS (planets) resolution:", true),
                        new DialogGUIHorizontalLayout(KSRSSGroup),
                        new DialogGUIFlexibleSpace(),
                        new DialogGUILabel("You MUST reboot KSP to apply the changes"),
                        new DialogGUIHorizontalLayout(new DialogGUIButton("Save", CloseDialog, false)),
                        new DialogGUIFlexibleSpace()
                    ));
            }
            popup = PopupDialog.SpawnPopupDialog(new Vector2(0.5f, 0.5f),
                new Vector2(0.5f, 0.5f),
                mod, false, HighLogic.UISkin, false);
        }
        
        private void Toggle4k(bool set)
        {
            if (set)
            {
                settingsNode.SetValue("textureRes", "4k");
                File.Move(patch4k, patch4kEnabled);
            }
            else
            {
                File.Move(patch4kEnabled, patch4k);
            }
            fileNode.Save(KSPUtil.ApplicationRootPath + "GameData/KSRSS/resolution.cfg");
        }
        
        private void Toggle8k(bool set)
        {
            if (set)
            {
                settingsNode.SetValue("textureRes", "8k");
            }
            fileNode.Save(KSPUtil.ApplicationRootPath + "GameData/KSRSS/resolution.cfg");
        }
        
        private void Toggle16k(bool set)
        {
            if (set)
            {
                settingsNode.SetValue("textureRes", "16k");
                File.Move(patch16k, patch16kEnabled);
            }
            else
            {
                File.Move(patch16kEnabled, patch16k);
            }
            fileNode.Save(KSPUtil.ApplicationRootPath + "GameData/KSRSS/resolution.cfg");
        }

        private void ToggleVE8k(bool set)
        {
            if (set)
            {
                settingsNode.SetValue("textureVERes", "8k");
            }
            fileNode.Save(KSPUtil.ApplicationRootPath + "GameData/KSRSS/resolution.cfg");
        }
        
        private void ToggleVE32k(bool set)
        {
            if (set)
            {
                settingsNode.SetValue("textureVERes", "32k");
                File.Move(patch32kVE, patch32kVEEnabled);
            }
            else
            {
                File.Move(patch32kVEEnabled, patch32kVE);
            }
            fileNode.Save(KSPUtil.ApplicationRootPath + "GameData/KSRSS/resolution.cfg");
        }

        public void CloseDialog()
        {
            if (popup == null)
                return;
            Vector3 rt = popup.RTrf.position;
            geometry = new Rect(
                rt.x / Screen.width  + 0.5f,
                rt.y / Screen.height + 0.5f,
                300f,
                100f
            );
            popup.Dismiss();
            popup = null;
        }
    }
}