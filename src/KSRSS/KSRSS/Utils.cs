﻿using UnityEngine;

namespace KSRSS
{
    public static class Utils
    {
        public static void Log(string msg)
        {
            Debug.Log("[KSRSS] " + msg);
        }
    }
}