# KSRSS

Kerbin Size Real Solar System

# Dependencies

[Kopernicus](https://github.com/Kopernicus/Kopernicus/releases/) and its dependencies

## If you want clouds and visual stuff : 

[Scatterer](https://forum.kerbalspaceprogram.com/index.php?/topic/103963-wip151-scatterer-atmospheric-scattering-v00336-08112018/)

[EnvironmentalVisualEnhancements](https://forum.kerbalspaceprogram.com/index.php?/topic/196411-19-111x-eve-redux-performance-enhanced-eve-maintenance-v11121-31012021/)

# How to change your texture resolution

If you have a low-end PC you may want to lower your texture resolution. And if you have a high-end PC maybe you want to melt your GPU with 32K textures. So how do you do this?

First you have to start KSP and wait until the main menu scene appears. You should see a window on the left. 

![KSRSS UI](https://i.imgur.com/VswtVTY.png)

Then, simply choose your resolution by clicking on a button, press Save and reboot KSP.

# Compatibility

Configs for FAR, KEX-Footprints, Sigma Binary, Distant Object Enhancement, CRP, PlanetShine, RationalResources and KSC-Switcher are included in KSRSS.

# Legal Info

Cfg files are licensed CC-BY-NC-SA 4.0, textures are All Rights Reserved unless otherwise stated and plugin code is GPLv3 with some licensed CC-BY-NC-SA 4.0 (see source files for details).

Copyright (c) KSRSS Team - 2020

KSRSS is derived from [RSS](https://forum.kerbalspaceprogram.com/index.php?/topic/177216-173-real-solar-system-v164-26-nov-2019/). A huge thanks to NathanKell and the RO team for creating this mod. (CC-BY-NC-SA-4.0)

67P and Halley are from [RSS Expansion](https://forum.kerbalspaceprogram.com/index.php?/topic/116275-105-rss-planets-moons-expanded-v0120-sedna-is-finally-here/) by pozine, licensed CC-BY 4.0. 

The grass and cactus textures are licensed MIT by Avera9eJoe and come from his mod, [Spectra](https://forum.kerbalspaceprogram.com/index.php?/topic/159443-16-spectra-v117-visual-compilation-1st-april-18/). All credit goes to him.

SigmaLoadingScreens is bundled with this mod and is covered by its own license.

Titan loading screen by Zarbon. Used with permission.

Toolbar Icon downloaded from [Icons8](https://icons8.com/).

## Textures in KSRSS/Textures/KSRSSVE

Aurora texture is from [Spectra](https://forum.kerbalspaceprogram.com/index.php?/topic/159443-12-181-spectra-v124-visual-compilation-10th-february-20/), licensed MIT by Avera9eJoe (Joseph Hafner).

Snow and dust particles and dust map (dust_mars.dds) are from [Sci-Fi Visual Enchancements](https://forum.kerbalspaceprogram.com/index.php?/topic/151190-16sci-fi-visual-enchancements-v-15-high-performance-alt-to-sve/) by panzer1b.

Snow map (snow.dds) comes from [Graphics Enhancements Assembly](https://forum.kerbalspaceprogram.com/index.php?/topic/161683-151-graphics-enhancements-assembly-gea/) (CC-BY-NC-SA 4.0).

Sunflare is from [here](https://spacedock.info/mod/997/Cepheus%20Sunflare) (CC-BY-NC-SA 4.0). All credits go to Thomassino for this wonderful sunflare.

Geysers textures (including rgb3.dds) and citylights textures come from [AVP](https://forum.kerbalspaceprogram.com/index.php?/topic/160878-ksp-181-astronomers-visual-pack-beyond-v401-21920/) (CC-BY-NC-SA 4.0). All credits go to themaster01 and Astronomer for these textures.

Mars, Earth, Venus, Jupiter, Saturn, Titan, Uranus, Neptune cloud textures, aurora detail texture, UVNoise textures, saturn ring texture and cloud particle textures are taken from [RSSVE](https://github.com/KSP-RO/RSSVE) by KSP RO Team (CC-BY-NC-SA 4.0) : 
  * Cloud and city detail textures by **[Real Visual Enhancements (RVE)](https://github.com/Pingopete)**
  * Earth cloud and city lights textures by **[NASA Visible Earth](http://visibleearth.nasa.gov)**
  * Jupiter texture (used for exporting some basic cloud textures) by **Vleider** (redistributed by **[The Celestia Motherlode](http://www.celestiamotherlode.net)**)
  * Saturn texture (used for exporting some basic cloud textures) by **Runar Thorvaldsen**, **Dr. Fridger Schremmp** and **Grant Hutchinson** (redistributed by **[The Celestia Motherlode](http://www.celestiamotherlode.net)**)
  * Saturn ring texture by **[CICLOPS](http://www.ciclops.org)** (Cassini Imaging Central Laboratory For Operations - image preparation by **Daren Wilson** and **Carolyn Porco**)
  * Uranus and Neptune textures (used for exporting some basic cloud textures) by **[JHT's Planetary Pixel Emporium](http://planetpixelemporium.com)**

All the others textures were made by the KSRSS Team and are licensed CC-BY-NC-SA 4.0.

## Textures in KSRSS/Textures/KSRSS

License: CC-BY-NC-SA 4.0 International

Copyright (c) KSRSS Team - 2020

### 8K pack

Includes artwork by dimonnomid and SpacedInvader as well as some by NathanKell.
Some planetary imagery is derived from work by Steve Albers and NASA / Jet Propulsion Laboratory, and some from the Celestia Motherlode (itself in the main sourced from JPL). Used by permission of the licenses for non-commercial release.

The Churyumov and Halley textures come from the [RSS Expansion](https://github.com/PhineasFreak/RSSExpansion) mod, originally by pozine, licensed CC-BY 4.0.
Many textures are from the [RSS-Textures repository](https://github.com/KSP-RO/RSS-Textures) licensed CC-BY-NC-SA by the KSP-RO team and were modified by the KSRSS Team.

Mimas, Tethys and Dione heightmaps by [Kexitt](https://www.deviantart.com/kexitt/art/Dione-Tethys-and-Mimas-Bump-Maps-647385511) based on [these models](https://3d-asteroids.space/moons/).

### 16K pack

16K textures are from [VaNnadin's 16K RSS texture pack](https://forum.kerbalspaceprogram.com/index.php?/topic/191018-vannadins-16k-rss-texture-pack-18x-support-update220-18-feb-2020/) (CC-BY-NC-SA 4.0).


Some planetary imagery is derived from work by Steve Albers and NASA / Jet Propulsion Laboratory, and some from the Celestia Motherlode (itself in the main sourced from JPL). Used by permission of the licenses for non-commercial release.

Ver.2.0.0 additional source
https://www.deviantart.com/master-bit/gallery/65534146/planetary-textures
https://www.deviantart.com/fargetanik/gallery/57723882/planetary-textures
https://www.deviantart.com/snowfall-the-cat/gallery/48193721/add-ons-and-files

For the others textures :

Includes artwork by dimonnomid and SpacedInvader as well as some by NathanKell.
Some planetary imagery is derived from work by Steve Albers and NASA / Jet Propulsion Laboratory, and some from the Celestia Motherlode (itself in the main sourced from JPL). Used by permission of the licenses for non-commercial release.

The Churyumov and Halley textures come from the [RSS Expansion](https://github.com/PhineasFreak/RSSExpansion) mod, originally by pozine, licensed CC-BY 4.0.
Many textures are from the [RSS-Textures repository](https://github.com/KSP-RO/RSS-Textures) licensed CC-BY-NC-SA by the KSP-RO team and were modified by the KSRSS Team.

